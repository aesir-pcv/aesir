# AESIR #



### What is AESIR? ###

**AESIR** (**A**utomated **E**nergy **S**urveillance & **I**nteractive **R**eports) is an energy management and monitoring system 
for buildings, environmental spaces, green energy devices, and electric vehicle (EV) charging pods & networks. 